import Vue from 'vue';
import VueRouter from 'vue-router';
import treeMenu from './menu';

Vue.use(VueRouter);

import apps from 'vdesk/apps';

import Dashboard from 'core/modules/dashboard/dashboard';

let routes = [{
  path: '/',
  component: Dashboard,
  name: 'home'
},];

// TODO: Переписать автогенерацию круда

import baseList from 'vdesk/modules/base/components/base-list';
import baseForm from 'vdesk/modules/base/components/base-form';

for (let appname in apps) {
  let app = apps[appname];
  if (app.crud) {
    let inline = (app.hasOwnProperty('inlineEdit')) ? app.inlineEdit : false,
     columns = (app.hasOwnProperty('columns')) ? app.columns : '_all_';

    try {
      let mod = require('vdesk/modules/' + appname + '/components/' + appname + '-list');
      routes.push({
        path: '/' + appname + '/',
        component: mod,
        name: appname + '.list'
      });
    } catch (e) {
      let mod = Vue.extend({
        extends: baseList,
        data: function () {
          return {
            model: API[appname],
            primaryKey: app['primaryKey'],
            route: appname,
            treeList: (app.treeList) ? app.treeList : false,
            title: (app.title) ? app.title : appname,
            inlineEdit: inline,
            columns: columns,
          };
        }
      });
      routes.push({
        path: '/' + appname + '/',
        component: mod,
        name: appname + '.list'
      });

      if (app.menu) {
        treeMenu.registerItem(app.menu.path, app.menu.name, {
          name: app.title,
          route: {
            name: appname + '.list'
          }
        });
      }
    }

    if (!inline) {
      try {
        let mod = require('vdesk/modules/' + appname + '/components/' + appname + '-form');
        routes.push({
          path: '/' + appname + '/edit/:id',
          component: mod,
          name: appname + '.edit'
        });
        routes.push({
          path: '/' + appname + '/add',
          component: mod,
          name: appname + '.add'
        });
      } catch (e) {
        let mod = Vue.extend({
          extends: baseForm,
          data: function () {
            return {
              model: API[appname],
              primaryKey: app['primaryKey'],
              route: appname,
              title: (app.title) ? app.title : appname,
              grid: (app.grid) ? app.grid : [],
            }
          }
        })
        routes.push({
          path: '/' + appname + '/edit/:id',
          component: mod,
          name: appname + '.edit'
        })
        routes.push({
          path: '/' + appname + '/add',
          component: mod,
          name: appname + '.add'
        })
      }
    }
  }
}



const router = new VueRouter({
  mode: 'history',
  base: 'app/',
  routes: routes // сокращённая запись для `routes: routes`
})

export {
  router,
  treeMenu
};
