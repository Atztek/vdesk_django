import Vue from 'vue';

import {
    TreeMenuPlugin,
    TreeMenu
} from 'core/plugins/tree-menu';

let menu = new TreeMenu();

Vue.use(TreeMenuPlugin);

// TODO: подумать о возможности переписать всё это для получения данных с бекенда.

// TODO: возможность задания путей меню в соответсвие с моделями

menu.registerItem(false, 'main', {
    name: 'Главная',
    icon: 'home',
    route: {
        name: 'home'
    }
});

menu.registerItem(false, 'characters', {
    name: 'Персонажи',
    icon: 'home',
});

export default menu;
