export default {

    'chr_character': {
      crud: true,
      primaryKey: 'chr_id',
      title: 'Персонажи',
      grid: [
        ['chr_name', 'xs2 md2:chr_lvl'],
        [{'xs6:abilities': [
            ['xs9:chab_ability', 'xs3:chab_value']
          ] } , [
            ['chr_age', 'chr_weight', 'chr_height'],
            ['chr_hair', 'chr_eyes'],
          ]
        ],
      ],
      columns: ['chr_name', 'chr_lvl'],
      menu: {
        path: 'characters',
        name: 'chr_character',
      }
    },

    'chr_ability': {
      crud: true,
      primaryKey: 'abl_id',
      title: 'Атрибуты',
      menu: {
        path: 'characters',
        name: 'chr_ability',
      }
    },

}
