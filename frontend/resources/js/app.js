require('./bootstrap');

import Vue from 'vue';
import Vuetify from 'vuetify';
import Vuex from 'vuex';
import ru from 'core/i18n/vuetify/ru';
import Meta from 'vue-meta';


Vue.use(Vuetify,{
  lang: {
    locales: {ru},
    current: 'ru'
  }
});
Vue.use(Vuex);
Vue.use(Meta);

require('./rest');

Vue.component(
'table-rest-list',
  require('core/main-components/crud/table-rest-list.vue')
);
Vue.component(
'form-rest-edit',
  require('core/main-components/crud/form-rest-edit.vue')
);
Vue.component(
'date-picker-menu',
  require('core/main-components/form/date-picker-menu.vue')
);
Vue.component(
'form-cell',
  require('core/main-components/crud/form-cell.vue')
);
Vue.component(
'form-layout',
  require('core/main-components/crud/form-layout.vue')
);
Vue.component(
'rest-form',
  require('core/main-components/crud/rest-form.vue')
);
Vue.component(
'related',
  require('core/main-components/crud/related.vue')
);

Vue.component(
'filtered-select',
  require('core/main-components/form/filtered-select.vue')
);

import {
  router,
  treeMenu
}

from 'vdesk/routes';


var app = new Vue({
  router,
  treeMenu,
  el: '#app',
  data: {
    drawer: true
  },
  metaInfo: {
    title: 'V - Desk',
  }
});
