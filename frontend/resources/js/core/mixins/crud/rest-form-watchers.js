export default {
    props: {
        watchers:{
            type: Object,
            default: ()=>{return {}}
        }
    },
    mounted:function(){
        for(let name in this.watchers){
            let callback = this.watchers[name];
            this.$watch(() => this.fieldsData[name], (val, old) => {
                if(typeof old == 'undefined'){ return; }
                callback.call(this, val, old);
            })
        }
    }
}

