export default{

  methods:{
      getValidationByField:function(field){
        return [
          v => !!v || 'Name is required',
          v => (v && v.length <= 10) || 'Name must be less than 10 characters'
        ]
      }
  }
}
