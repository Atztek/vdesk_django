export default {
    props: {
      // primary field name
      primaryKey: {
        type: String,
        required: true,
      },
      // REST API model
      model: {
        type: [Object, Function],
        required: true,
      },
      // message model
      base_message_model:{
        type: String,
        required: false,
      },
      // navigation route
      route: {
        type: String,
        required: true,
      },
      // page title
      title: {
        type: String,
        required: false,
        default: "CRUD",
      },
      grid: {
        type: Array,
        default: ()=>{return [];}
      }
    },
    mounted:function(){
      this.getOptions();
      this.fetchData();
      if(!this.base_message_model){
        this.message_model = _.findKey(API, (item) => {
          return item.defaults.baseURL == this.model.defaults.baseURL
        })
      }else{
        this.message_model = this.base_message_model;
      }
    },
    watch: {
      // при изменениях маршрута запрашиваем данные снова
      '$route': 'fetchData'
    },
    data:function(){
      return {
        message_model: '',
        headers:[],
        items:[],
        loading:false,
      }
    },
    computed:{
      routeEdit:function(){
        return this.route + '.edit';
      },
      routeAdd:function(){
        return this.route + '.add';
      },
      routeList:function(){
        return this.route + '.list';
      },
    },
}
