export default {
    data: function(){
        return {
            elements: {
                'filtered-select': 'filtered-select',
                'choice': 'v-select',
                'boolean': 'v-checkbox',
                'date': 'date-picker-menu',
                'string': 'v-text-field',
                'integer': 'v-text-field',
                'float': 'v-text-field',
                'field': 'related',
                //'field': 'v-text-field',
                'nested object': 'rest-form',
                //'nested object': 'v-text-field',
            }
        }
    },
    methods:{
        loadFiledByType: function (field, grid) {
            let type = (grid['field_type']) ? grid['field_type'] : field.type;
            return this.elements[type];
        },
    }
}