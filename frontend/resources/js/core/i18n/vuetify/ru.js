export default {
  dataIterator: {
    rowsPerPageText: 'Записей на страницу:',
    rowsPerPageAll: 'Все',
    pageText: '{0}-{1} из {2}',
    noResultsText: 'По запросу ничего не найдено',
    nextPage: 'Следующея страница',
    prevPage: 'предыдущая страница'
  },
  dataTable: {
    rowsPerPageText: 'Записей на страницу:'
  },
  noDataText: 'Нет данных',
  actions: 'Действия',
}
