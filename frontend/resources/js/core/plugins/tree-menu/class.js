var _ = require('lodash');
import Vue from 'vue';

class TreeMenu {
    constructor(data) {
        if (typeof data == 'undefined') {
            data = {
                root: true
            };
        }
        this.name = (data.hasOwnProperty('name')) ? data.name : '';
        this.icon = (data.hasOwnProperty('icon')) ? data.icon : '';
        this.root = (data.hasOwnProperty('root')) ? data.root : false;
        this.route = (data.hasOwnProperty('route'))? data.route: null;
        this.state = false;
        this.items = {};
    }

    haveChilds(){
        return _.keys(this.items).length > 0;
    }

    registerItem(path, name, data) {
        let root = this.getItemByPath(path);
        root.addItem(name, data);
    }

    addItem(name, data){
        Vue.set(this.items, name, new TreeMenu(data));
    }

    getItemByPath(path) {
        if (path == false || typeof path == 'undefined'){
            return this;
        }

        if(typeof path == 'string'){
            path = path.split('.');
        }

        let part = path.shift();

        if (this.items[part]){
            return this.items[part].getItemByPath(path);
        }

    }
}

export default TreeMenu;
