export default {

    install: function (Vue, options) {

        Vue.mixin({
            beforeCreate() {
                if (this.$options.treeMenu) {
                    this._treeMenu = this.$options.treeMenu;
                    Vue.util.defineReactive(this, '_treeMenu', this.$treeMenu);
                } else if (this.$options.parent && this.$options.parent._treeMenu) {
                    this._treeMenu = this.$options.parent._treeMenu;
                }
            }
        });

        Object.defineProperty(Vue.prototype, '$treeMenu', {
            get() {
                return this._treeMenu;
            }
        });

        Vue.component('left-menu', require('./components/left-menu'));
        Vue.component('left-menu-item', require('./components/left-menu-item'));

    },
};