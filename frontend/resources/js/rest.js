
window.API = {};


import autoGentRoutes from 'vdesk/apps'



for(var name in autoGentRoutes){
  const tempRest = axios.create({
    // (process.env.NODE_ENV=='development')?'http://127.0.0.1:'+autoGentRoutes[name].port+'/api/'+name+'/':
    baseURL: '/api/'+name+'/',
    timeout: 1000,
  });
  tempRest.interceptors.request.use((config) => {
    if ( config.url && config.url[config.url.length-1] !== '/') {
      config.url += '/';
    }
    return config;
  });

  window.API[name] = tempRest;
}

//window.REST.railway = require('karat_c/modules/railway/rest/railway');
