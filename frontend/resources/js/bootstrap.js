window.axios = require('axios');

var _ = require('lodash');

import { DateTime , Interval} from 'luxon';

window.DateTime = DateTime
window.Interval = Interval

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */


window.uuidv4 = function (){
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

let token = document.head.querySelector('meta[name="csrf-token"]');
window.token = token

if(process.env.NODE_ENV=='development'){
  // window.axios.defaults.headers.common['Access-Control-Request-Headers'] = '*';
}

if (token) {
    window.axios.defaults.headers.common['X-CSRFToken'] = token.content;

} else {
    console.error('CSRF token not found');
}
