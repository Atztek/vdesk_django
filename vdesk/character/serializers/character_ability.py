from django.utils.translation import ugettext_lazy as _

from baseapp.rest import BaseModelSerializer
from character.models import CharacterAbility


class CharacterAbilitySerializer(BaseModelSerializer):
    """A serializer of CharacterAbility instances."""

    class Meta:
        model = CharacterAbility
        fields = ['chab_ability', 'chab_id', 'chab_value']
        extra_kwargs = {'chab_value': {'required': True, 'allow_null': False}}
