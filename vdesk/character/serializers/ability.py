from django.utils.translation import ugettext_lazy as _

from baseapp.rest import BaseModelSerializer
from character.models import Ability

class AbilitySerializer(BaseModelSerializer):
    """A serializer of Ability instances."""
    class Meta:
        model = Ability
        fields = '__all__'
