from django.utils.translation import ugettext_lazy as _

from baseapp.rest import BaseModelSerializer
from character.models import Character

from .character_ability import CharacterAbilitySerializer
from rest_framework import serializers

from collections import Counter


class CharacterSerializer(BaseModelSerializer):
    # abilities = CharacterAbilitySerializer(many=True, label="Атрибуты")

    # def validate(self, data):
    #    uids = [ab['chab_ability'] for ab in data['abilities']]
    #    if [k for k, v in Counter(uids).items() if v > 1]:
    #        raise serializers.ValidationError({
    #            'abilities': {'non_field_errors': _('Атрибуты не должны повторяться')}
    #        })
    #    return data

    """A serializer of Character instances."""
    class Meta:
        model = Character
        fields = '__all__'
