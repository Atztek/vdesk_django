from django.db import models
import uuid
from django.utils.translation import ugettext_lazy as _


class Ability(models.Model):
    """Ability model."""

    abl_id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                              editable=False, verbose_name="UUID",)
    abl_name = models.CharField(max_length=100,
                                verbose_name=_("Название атрибута"))
    abl_shortname = models.CharField(max_length=100,
                                     verbose_name=_("Сокращенное название"))
    abl_descriptiton = models.TextField(blank=True, verbose_name=_("Описание"))

    class Meta:
        verbose_name = _("Атрибут")
        verbose_name_plural = _("Атрибуты")
        app_label = "character"

    def __str__(self):
        return self.abl_name
