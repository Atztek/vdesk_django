from django.db import models
import uuid
from django.utils.translation import ugettext_lazy as _


class Character(models.Model):
    """Character model."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                          editable=False, verbose_name="UUID",)
    name = models.CharField(max_length=100, verbose_name=_("Имя"))
    lvl = models.PositiveSmallIntegerField(default=1,
                                           verbose_name=_("Уровень"))
    age = models.PositiveSmallIntegerField(default=18,
                                           verbose_name=_("Возраст"))
    height = models.PositiveSmallIntegerField(default=18,
                                              verbose_name=_("Рост"))
    weight = models.PositiveSmallIntegerField(default=18,
                                              verbose_name=_("Вес"))
    hair = models.CharField(max_length=100, verbose_name=_("Цвет волос"))
    eyes = models.CharField(max_length=100, verbose_name=_("Цвет глаз"))

    class Meta:
        verbose_name = _("Персонаж")
        verbose_name_plural = _("Персонажи")
        app_label = "character"

    def __str__(self):
        return self.chr_name
