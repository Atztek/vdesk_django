from django.db import models
import uuid
from django.utils.translation import ugettext_lazy as _

from .ability import Ability
from .character import Character

class CharacterAbility(models.Model):
    """CharacterAbility model. """

    chab_id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                          editable=False, verbose_name="UUID",)
    chab_ability = models.ForeignKey(Ability, on_delete=models.CASCADE,
                                     verbose_name="Атрибут")
    chab_character = models.ForeignKey(Character, on_delete=models.CASCADE,
                                       verbose_name=_("Персонаж"),
                                       related_name='abilities')
    chab_value = models.PositiveSmallIntegerField(default=18,
                                                  verbose_name=_("Значение"))


    class Meta:
        unique_together = (("chab_character", "chab_ability"),)

        verbose_name = _("Атрибут персонажа")
        verbose_name_plural = _("Атрибуты персонажа")
        app_label = "character"


    def __str__(self):
        return f'{self.chab_ability} - {self.chab_value}'
