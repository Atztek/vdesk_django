from rest_framework import viewsets
from character.serializers import AbilitySerializer

from character.models import Ability
from baseapp.rest import ChoiceMetaData


class AbilityViewSet(viewsets.ModelViewSet):
    """A viewset for viewing and editing Character instances."""
    metadata_class = ChoiceMetaData
    serializer_class = AbilitySerializer
    queryset = Ability.objects.all()
