from rest_framework import viewsets
from character.serializers import CharacterSerializer

from character.models import Character
from baseapp.rest import ChoiceMetaData


class CharacterViewSet(viewsets.ModelViewSet):
    """A viewset for viewing and editing Character instances."""
    metadata_class = ChoiceMetaData
    serializer_class = CharacterSerializer
    queryset = Character.objects.all()
