
import re

def get_model_rest(model): 
    if hasattr(model, 'rest'):
        return model.rest
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', model.__name__)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()
