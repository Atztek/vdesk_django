from rest_framework.decorators import action
from rest_framework.response import Response

from rest_framework import viewsets

from django.apps import apps
from rest_framework import filters


class BaseModelViewset(viewsets.ModelViewSet):
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = '__all__'


class SearchMixin():

    @action(detail=False, methods=['post'])
    def search(self, request):
        query_dict = self.parse_query(request.data)
        query = self.get_queryset().filter(**query_dict)
        serializer = self.get_serializer(query, many=True)
        return Response(serializer.data)

    def parse_query(self, query_dict):
        query = {}

        for key, val in query_dict.items():

            pkey, is_model = self.check_key(key)

            if not is_model:
                preparedval = self.parse_val(val)
                query.update({pkey: preparedval})
            else:
                extract = val.pop('extract', 'pk')
                preparedval = self.parse_val(val)
                return pkey.objects.filter(**preparedval).values_list(extract, flat=True)

            # print(key)

        return query

    def parse_val(self, val):

        if isinstance(val, (str, int, float, list)):
            return val

        if isinstance(val, dict):
            return self.parse_query(val)

        return False

    def check_key(self, key):
        parts = key.split('.')

        if len(parts) == 1:
            return key, False
        else:
            return apps.get_model(*parts), True
