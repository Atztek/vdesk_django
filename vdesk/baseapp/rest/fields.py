from rest_framework import serializers
import json


class CPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    """ """

    def use_pk_only_optimization(self):
        return False


class CJSONField(serializers.JSONField):
    """docstring for CJSONField."""

    def to_representation(self, value):
        return value

    def to_internal_value(self, data):
        if self.binary and isinstance(data, dict):
            return data
        try:
            data = json.loads(data)
        except (TypeError, ValueError) as e:
            self.fail('invalid')
        return data
