from rest_framework import permissions
from django.core.exceptions import ObjectDoesNotExist

class IsDelEditPermissions(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """
    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        methodMap = {'DELETE': 'ide_prevent_del', 'PUT': 'ide_prevent_edit'}
        attr = methodMap.get(request._request.method, False)
        if attr:
            try:
                perm = obj.rowperm.get()
                return not getattr(perm, attr)
            except ObjectDoesNotExist as e:
                return True

            return False
        # Instance must have an attribute named `owner`.
        return True
