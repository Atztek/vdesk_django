from rest_framework import serializers
from rest_framework.serializers import raise_errors_on_nested_writes
from collections import OrderedDict
from rest_framework.fields import SkipField
from rest_framework.relations import PKOnlyObject
from django.db.models import Model
from baseapp.rest.models import IsDelEdit
from .fields import CPrimaryKeyRelatedField
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.utils import model_meta
from django.db.models.fields import related
from django.contrib.contenttypes.fields import GenericRelation

class IsDelEditRelatedField(serializers.RelatedField):
    """A custom field to use for the `tagged_object` generic relationship."""
    def get_queryset(self):
        return IsDelEdit.objects.all()

    def to_internal_value(self, data):
        pass

    def to_representation(self, value):
        """Serialize tagged objects to a simple textual representation."""
        try:
            row = value.get()
            return {
                'is_del': not row.ide_prevent_del,
                'is_edit': not row.ide_prevent_edit,
            }

        except Exception:
            return {'is_del':True, 'is_edit': True}

        raise Exception('Unexpected type of tagged object')


class BaseListSerializer(serializers.ListSerializer):
    """docstring for ."""

    def bulk_update(self, queryset):
        """Пакетное обновление"""
        pk = self.child.getModelPK()
        for item in self.initial_data:
            try:
                instace = queryset.get(pk=item[pk])
            except Exception as e:
                instace = queryset.model(pk=item[pk])

            data = self.child.run_validation(data=dict(item))
            innst = self.child.update(instace, data)


from rest_framework.fields import (  # NOQA # isort:skip
   empty
)
class BaseModelSerializer(serializers.ModelSerializer):
    """Переопределение ModelSerializer для вывода сваязаного поля в виде строки."""

    def ext_update(self, queryset, data):
        pk = self.getModelPK()

        try:
            instace = queryset.get(pk=data[pk])
            innst = self.update(instace, data)
        except ObjectDoesNotExist as e:
            self.create(data)

    def getModelPK(self, model=False):
        if model:
            return model._meta.pk.name
        return self.Meta.model._meta.pk.name

    def is_related(self):
        return not bool(self._context)

    def to_internal_value(self, data):
        """Override method for getting pk key, value on update in related serializers."""
        pk_dict = {}

        if self.is_related():
            pkname = self.getModelPK()
            if pkname in data:
                pk_dict[pkname] = data.pop(pkname)
            else:
                pk_dict[pkname] = False

        ret = super().to_internal_value(data)
        ret.update(pk_dict)
       
        return ret



    def create(self, validated_data):

        ModelClass = self.Meta.model
        # Remove many-to-many relationships from validated_data.
        # They are not valid arguments to the default `.create()` method,
        # as they require that the instance has already been saved.
        info = model_meta.get_field_info(ModelClass)

        generic_relations = {}
        for field in ModelClass._meta._get_fields():
            if isinstance(field, GenericRelation) and field.name in validated_data:
                generic_relations[field.name] = (
                    validated_data.pop(field.name), field.related_model
                )

        many_to_many = {}
        reverse_rel = {}
        for field_name, relation_info in info.relations.items():
            if (relation_info.to_many and not relation_info.reverse) and (field_name in validated_data):
                many_to_many[field_name] = validated_data.pop(field_name)

            if (relation_info.to_many and relation_info.reverse) and (field_name in validated_data):
                
                reverse_rel[field_name] = (
                    validated_data.pop(field_name), relation_info.related_model)
                

            if (field_name in validated_data
                    and not isinstance(validated_data[field_name], Model)
                    and isinstance(relation_info.model_field, related.ForeignKey)
                    and not relation_info.to_many
                    and (field_name in validated_data)
                    and validated_data[field_name]):
                validated_data[field_name] = relation_info.related_model.objects.get(pk=validated_data[field_name])

        try:
            instance = ModelClass._default_manager.create(**validated_data)
        except TypeError:
            raise TypeError("BaseSerrializer TypeError")

        # Save many-to-many relationships after the instance is created.
        if many_to_many:
            for field_name, value in many_to_many.items():
                field = getattr(instance, field_name)
                field.set(value)

        if reverse_rel:
            for field_name, (data, model) in reverse_rel.items():
                relt_field_name = getattr(ModelClass, field_name).field.name
                pk_field_name = self.getModelPK(
                    getattr(instance, field_name).model)
                for i in data:
                    i.update({relt_field_name: instance})
                    i.pop(pk_field_name)

                getattr(instance, field_name).bulk_create(
                    [model(** x) for x in data])

        if generic_relations:
            for field_name, (data, model) in generic_relations.items():
                if hasattr(model,'append_to_instance'):
                    model.append_to_instance(instance, data)
                else:
                    raise NotImplementedError(
                            "method `append_to_instance` not implement in "
                            " %s, model. " % (model)
                        )

        return instance

    def update(self, instance, validated_data):
        #raise_errors_on_nested_writes('update', self, validated_data)
        info = model_meta.get_field_info(instance)
        # Simply set each attribute on the instance, and then save it.
        # Note that unlike `.create()` we don't need to treat many-to-many
        # relationships as being a special case. During updates we already
        # have an instance pk for the relationships to be associated with.
        ModelClass = self.Meta.model
        info = model_meta.get_field_info(ModelClass)

        generic_relations = {}
        for field in ModelClass._meta._get_fields():
            if isinstance(field, GenericRelation) and field.name in validated_data:
                generic_relations[field.name] = (
                    validated_data.pop(field.name), field.related_model
                )
        
        many_to_many = {}
        reverse_rel = {}
        for field_name, relation_info in info.relations.items():
            if (relation_info.to_many and not relation_info.reverse) and (field_name in validated_data):
                many_to_many[field_name] = validated_data.pop(field_name)

            if (relation_info.to_many and relation_info.reverse) and (field_name in validated_data):
                
                reverse_rel[field_name] = (
                    validated_data.pop(field_name), relation_info.related_model)
                

            if (field_name in validated_data
                    and not isinstance(validated_data[field_name], Model)
                    and isinstance(relation_info.model_field, related.ForeignKey)
                    and not relation_info.to_many
                    and (field_name in validated_data)
                    and validated_data[field_name]):
                validated_data[field_name] = relation_info.related_model.objects.get(pk=validated_data[field_name])

        for attr, value in validated_data.items():
            if attr in info.relations and info.relations[attr].to_many:
                field = getattr(instance, attr)
                field.set(value)
            elif (attr in info.relations
                    and isinstance(info.relations[attr].model_field,
                                   related.ForeignKey)
                    and not info.relations[attr].to_many
                    and (attr in validated_data)
                    and validated_data[attr]):
                validated_data[attr] = info.relations[attr].related_model.objects.get(pk=validated_data[attr])
            else:
                setattr(instance, attr, value)
        instance.save()

        if reverse_rel:
            for field_name, (data, model) in reverse_rel.items():
                relt_field_name = getattr(ModelClass, field_name).field.name
                pk_field_name = self.getModelPK(getattr(instance, field_name).model)
                
                usedPk = []
                for row in data:
                    pk = row.pop(pk_field_name, False)
                    if pk:
                        item = getattr(instance, field_name).get(pk=pk)
                        for attr, val in row.items():
                            setattr(item, attr, val)
                        item.save()
                        usedPk.append(item.pk)
                    else:
                        item = getattr(instance, field_name).create(**row)
                        usedPk.append(item.pk)
                
                getattr(instance, field_name).exclude(
                    pk__in=usedPk).delete()
                
                #    i.update({relt_field_name: instance})
                #getattr(instance, field_name).bulk_create(
                #    [model(** x) for x in data])

        if generic_relations:
            for field_name, (data, model) in generic_relations.items():
                if hasattr(model,'append_to_instance'):
                    model.append_to_instance(instance, data)
                else:
                    raise NotImplementedError(
                            "method `append_to_instance` not implement in "
                            " %s, model. " % (model)
                        )

        return instance

    # Determine the fields to apply..
    @classmethod
    def many_init(cls, *args, **kwargs):
        """
        This method implements the creation of a `ListSerializer` parent
        class when `many=True` is used. You can customize it if you need to
        control which keyword arguments are passed to the parent, and
        which are passed to the child.
        Note that we're over-cautious in passing most arguments to both parent
        and child classes in order to try to cover the general case. If you're
        overriding this method you'll probably want something much simpler, eg:
        @classmethod
        def many_init(cls, *args, **kwargs):
            kwargs['child'] = cls()
            return CustomListSerializer(*args, **kwargs)
        """
        allow_empty = kwargs.pop('allow_empty', None)
        child_serializer = cls(*args, **kwargs)
        list_kwargs = {
            'child': child_serializer,
        }
        if allow_empty is not None:
            list_kwargs['allow_empty'] = allow_empty
        list_kwargs.update({
            key: value for key, value in kwargs.items()
            if key in serializers.LIST_SERIALIZER_KWARGS
        })
        meta = getattr(cls, 'Meta', None)
        list_serializer_class = getattr(meta, 'list_serializer_class', BaseListSerializer)
        return list_serializer_class(*args, **list_kwargs)

    def to_representation(self, instance):
        """Object instance -> Dict of primitive datatypes."""
        ret = OrderedDict()
        fields = self._readable_fields
        for field in fields:
            try:
                attribute = field.get_attribute(instance)
            except SkipField:
                continue

            # We skip `to_representation` for `None` values so that fields do
            # not have to explicitly deal with that case.
            #
            # For related fields with `use_pk_only_optimization` we need to
            # resolve the pk value.
            check_for_none = attribute.pk if isinstance(attribute, PKOnlyObject) else attribute
            if check_for_none is None:
                ret[field.field_name] = None
            else:
                # выводить pk связаного обьекта вместо значения
                PKOnly = 'view' not in self.context or self.context['view'].request.GET.get('PKOnlyObject', False)
                if isinstance(field, CPrimaryKeyRelatedField) and not PKOnly:
                    ret[field.field_name] = field.display_value(attribute)
                else:
                    ret[field.field_name] = field.to_representation(attribute)

        return ret
