from django.db import models
import uuid
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
# Create your models here.


class IsDelEdit(models.Model):
    ide_id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                              editable=False, verbose_name="UUID",)
    ide_prevent_del = models.BooleanField(default=True)
    ide_prevent_edit = models.BooleanField(default=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.UUIDField(editable=False, verbose_name="UUID")
    content_object = GenericForeignKey('content_type', 'object_id')

    @staticmethod
    def append_to_instance(instance, data):
        if not data['is_del'] or not data['is_edit']:
            inst = IsDelEdit.objects.get_or_create(
                content_object=instance
            )
            inst.ide_prevent_del = data['is_del']
            inst.ide_prevent_edit = data['is_edit']
            inst.save()
            return inst
