from django.utils.encoding import force_text
from rest_framework.metadata import SimpleMetadata
from rest_framework.relations import ManyRelatedField, RelatedField
from .serializers import IsDelEditRelatedField
from collections import OrderedDict
from .utils import get_model_rest

class ChoiceMetaData(SimpleMetadata):
    """Класс метадаты для вывода списка значений связаной модели."""

    def determine_metadata(self, request, view):
        """Переопределение базового получения метаданных."""

        # флаг указывающи на получение списка для поля RelatedField
        self.withChoice = view.request.data.get('withChoice', False)
        metadata = OrderedDict()
        metadata['name'] = view.get_view_name()
        metadata['description'] = view.get_view_description()
        # сокрытие лишней информации
        # metadata['renders'] = [renderer.media_type for renderer in view.renderer_classes]
        # metadata['parses'] = [parser.media_type for parser in view.parser_classes]
        if hasattr(view, 'get_serializer'):
            actions = self.determine_actions(request, view)
            if actions:
                metadata['actions'] = actions
        return metadata

    def get_field_info(self, field):
        """Получений значений списка."""
        field_info = super(ChoiceMetaData, self).get_field_info(field)

        if isinstance(field, IsDelEditRelatedField):
            field_info['type'] = 'hidden'
            return field_info

        if isinstance(field, (RelatedField, ManyRelatedField)):
            field_info['type'] = 'choice'
        
            field_info['search_model'] = get_model_rest(field.queryset.model)

            if not self.withChoice:
                return field_info

            field_info['choices'] = [
                {
                    'value': choice_value,
                    'display_name': force_text(choice_name, strings_only=True)
                }
                for choice_value, choice_name in field.choices.items()
            ]
        return field_info
