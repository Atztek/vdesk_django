from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _

from django.contrib.auth.validators import ASCIIUsernameValidator, UnicodeUsernameValidator
import uuid


class User(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                          editable=False, verbose_name="UUID",)

    class Meta:
        verbose_name = _('Пользователь')
        verbose_name_plural = _('Пользователи')
